import {
    reducerDestinosviajes,
    DestinosviajesState,
    intializeDestinosviajesState,
    InitMyDataAction,
    NuevoDestinoAction
} from './destinos-viajes-state.model';
import {Destinoviaje} from './destino-viaje.model';

describe('reducerDestinosViajes', ()=> {
    it('should reduce init data', () => {
        //setup
        const prevState: DestinosviajesState = intializeDestinosviajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1','destino 2']);
        //action
        const newState: DestinosviajesState= reducerDestinosviajes(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
        //tear down
    });
    it('should reduce new item added', () => {
        const prevState: DestinosviajesState = intializeDestinosviajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new Destinoviaje('barcelona', 'url'));
        const newState: DestinosviajesState= reducerDestinosviajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});