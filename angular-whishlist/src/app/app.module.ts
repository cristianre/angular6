import { BrowserModule } from '@angular/platform-browser';
import { Injectable, InjectionToken, NgModule, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; 
import {StoreModule as NgRxStoreModule, ActionReducerFactory, Store} from '@ngrx/store';

import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {HttpClientModule, HttpHeaders, HttpRequest, HttpClient, HttpResponse} from '@angular/common/http';
import  Dexie from 'dexie';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { DestinoviajeComponent } from './components/destinoviaje/destinoviaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { InitMyDataAction, DestinosviajesEffects, DestinosviajesState, intializeDestinosviajesState, reducerDestinosviajes } from './models/destinos-viajes-state.model';
import { ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import {UsuarioLogueadoGuard} from './guards/usuario-logueado/usuario-logueado.guard';
import {AuthService} from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasListadoComponent } from './reservas/reservas-listado/reservas-listado.component';
import { ReservasDetalleComponent } from './reservas/reservas-detalle/reservas-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { Destinoviaje } from './models/destino-viaje.model';
import { RecurseVisitor } from '@angular/compiler/src/i18n/i18n_ast';
import { from, Observable } from 'rxjs';
import {map, flatMap} from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// app config 
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');


export const childrenRoutesVuelos: Routes =[
  {path: '', redirectTo: 'main', pathMatch:'full'},
  {path: 'main', component: ListaDestinosComponent},
  {path: 'mas-info', component: VuelosMasInfoComponentComponent},
  {path: ':id', component: VuelosDetalleComponent},
];

const routes: Routes = [
  {path:'',redirectTo:'home', pathMatch:'full'},
  {path:'home',component:ListaDestinosComponent},
  {path:'destino/:id',component:DestinoDetalleComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]

  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate:[UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }
];

//redux init
export interface AppState{
  destinos:DestinosviajesState;
}
///redux fin init
const reducers: ActionReducerMap<AppState> ={
  destinos:reducerDestinosviajes
};

const reducersInitialState ={
  destinos:intializeDestinosviajesState()
};
//app init 
export function init_app(appLoadService: AppLoadService):() => Promise<any> {
  return () => appLoadService.intializeDestinosviajesState();
}
@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http:HttpClient) { }
  async intializeDestinosviajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API_TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers:headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  } 
}
//fin app init

//dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie{
  destinos: Dexie.Table<Destinoviaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imageUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}
export const db = new MyDatabase();
//fin

//i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) {}
  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
    .where('lang')
    .equals(lang)
    .toArray()
    .then(results =>{
      if (results.length === 0) {
        return this.http
        .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint +'/api/translation?lang=' + lang)
        .toPromise()
        .then(apiResults => {
          db.translations.bulkAdd(apiResults);
          return apiResults;
        });
      }
      return results;
    }).then((traducciones) => {
      console.log('traducciones cargadas:');
      console.log (traducciones);
      return traducciones;
    }).then((traducciones) => {
      return traducciones.map((t) => ({[t.key]: t.value}));
    });
    return from(promise).pipe(flatMap((elems) => from (elems)));
  }
}

@NgModule({
  declarations: [
    AppComponent,
    DestinoviajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponent,
    ReservasListadoComponent,
    ReservasDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers,{initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosviajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps:[HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
     AuthService, UsuarioLogueadoGuard,
     {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
     AppLoadService,
     {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
