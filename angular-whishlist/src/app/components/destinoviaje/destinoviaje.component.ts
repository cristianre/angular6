import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import { Destinoviaje } from './../../models/destino-viaje.model';
import {AppState} from '../../app.module';
import {Store} from '@ngrx/store';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
import {trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css'],
  animations:[
    trigger('esFavorito', [
      state('estadoFavorito', style ({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style ({
        backgroundColor: 'whiteSmoke'
      })),
      transition ('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]), 
    ])
  ]
})
export class DestinoviajeComponent implements OnInit {
  @Input() destino: Destinoviaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = "col-md-4";
  @Output() onClicked:EventEmitter<Destinoviaje>;

  constructor(private store: Store<AppState>) { 
    this.onClicked=new EventEmitter();
  }

  ngOnInit(): void {
  }
ir(){
  this.onClicked.emit(this.destino);
  return false;

}
voteUp(){
  this.store.dispatch(new VoteUpAction(this.destino));
  return false;
}
voteDown(){
  this.store.dispatch(new VoteDownAction(this.destino));  
  return false;
}
}
