import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Destinoviaje } from './../../models/destino-viaje.model';
import {DestinosApiClient} from '../../models/destinos-api-client.model';
import {Store} from '@ngrx/store';
import { AppState } from '../../app.module';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient],
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Destinoviaje>;
  updates:string[];


  constructor(private destinosApiClient:DestinosApiClient,private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates=[];
   }
    ngOnInit(){
      this.store.select(state => state.destinos.favorito)
      .subscribe(data =>{
        const f=data;
        if (f != null){
          this.updates.push('se ha elegido a'+ f.nombre);
        }
      });
    }
    agregado(d:Destinoviaje){
      this.destinosApiClient.add(d);
      this.onItemAdded.emit(d);      
    }
    elegido(d: Destinoviaje){
    this.destinosApiClient.elegir(d);
    
  }
  getAll(){

  }
}
